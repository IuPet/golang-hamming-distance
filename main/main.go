package main

import (
	"fmt"
)

func main() {
	var stringDNA1, stringDNA2 string

	stringDNA1 = "1AGCCTA"
	stringDNA2 = "GAGCCTA"
	fmt.Printf("%s and %s: %d\n", stringDNA1, stringDNA2, stringDistance(stringDNA1, stringDNA2))

	stringDNA1 = "GAGCCTA"
	stringDNA2 = "GAGCCTACTAACGGGAT"
	fmt.Printf("%s and %s: %d\n", stringDNA1, stringDNA2, stringDistance(stringDNA1, stringDNA2))

	stringDNA1 = "GAGCCTACTAACGGGAT"
	stringDNA2 = "GAGCCTACTAACGGGAT"
	fmt.Printf("%s and %s: %d\n", stringDNA1, stringDNA2, stringDistance(stringDNA1, stringDNA2))

	stringDNA1 = "GAGCCTACTAACGGGAT"
	stringDNA2 = "CATCGTAATGACGGCCA"
	fmt.Printf("%s and %s: %d\n", stringDNA1, stringDNA2, stringDistance(stringDNA1, stringDNA2))

}
